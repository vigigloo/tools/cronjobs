{{/*
Generate unique fullname for each cronjob
*/}}
{{- define "cronjob.fullname" -}}
{{- $topLevelScope := index . 0 -}}
{{- $cronjob := index . 1 -}}
{{- printf "%s-%s" (include "common.fullname" $topLevelScope) $cronjob.name | trunc 63 | trimSuffix "-" }}
{{- end }}
